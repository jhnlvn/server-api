const Product = require("../Models/productsSchema.js");
const auth = require("../auth.js");
const mongoose = require("mongoose");


/*
	PRODUCT MANIPULATIONS
*/

module.exports.addProduct = (request, response) =>{
	let input = request.body;
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin) {
		let newItem = new Product({
			name: input.name,
			category: input.category,
			description: input.description,
			price: input.price,
			stocks: input.stocks,
			image: input.image
		});

		return newItem.save()
		.then(product => response.send(product))
		.catch(error => response.send(false))
	}
	else {
		return response.send(false);
	}
}

module.exports.updateProduct = (request, response) => {
	let product = request.body;
	const id = request.params.id
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin) {
		Product.findOneAndUpdate({_id: id},{
			name : product.name,
			category : product.category,
			description: product.description,
			price: product.price,
			stocks : product.stocks,
			itemDiscount : product.discount
		},{new:true})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
	else {
		return response.send(false);
		}
}

module.exports.archiveProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const id = request.params.id;

	if(userData.isAdmin) {
		Product.findByIdAndUpdate({_id: id},{stocks: 0},{new:true})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
	else {
		return response.send(false);
	}
}

/*
	VIEW PRODUCTS
*/

module.exports.viewProducts = (request, response) => {
	Product.find({stocks: {$ne: 0}})
	.then(result => response.send(result))
	.catch(error => response.send(false))
}

module.exports.viewCategory = (request, response) => {
	const itemCategory = request.params.category;

	Product.find({category: itemCategory,stocks: {$ne: 0}})
	.then(result => response.send(result))
	.catch(error => response.send(false))
}

module.exports.viewArchives = (request, response) => {
	Product.find({stocks: 0})
	.then(result => response.send(result))
	.catch(error => response.send(false))
}

module.exports.viewItem = (request, response) => {
	const _id = request.params.id;

	Product.findById({_id})
	.then(result => response.send(result))
	.catch(error => response.send(false))
}

module.exports.searchProducts = (request, response) => {
	const products = request.params.itemName;

	Product.find({name: {$regex: products, $options: "$i"},stocks: {$ne: 0}})
	.then(result => response.send(result))
	.catch(error => response.send(false))
}