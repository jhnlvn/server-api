const Coupon = require("../Models/couponsSchema.js");
const auth = require("../auth.js");
const mongoose = require("mongoose");

/*
	COUPONS
*/

module.exports.addCoupon = (request, response) =>{
	const userData = auth.decode(request.headers.authorization);
	let input = request.body;

	if(userData.isAdmin) {
		let newCoupon = new Coupon({
			couponCode: input.coupon,
			couponDiscount : input.discount
		})

		newCoupon.save()
		.then(product => response.send(product))
		.catch(error => response.send(false))
	}
	else {
		return response.send(false);
	}
}

module.exports.viewCoupons = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin) {
		Coupon.find({})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
	else {
		return response.send(false);
	}
}

module.exports.updateCoupon = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const input = request.body;
	const code = request.params.code

	if(userData.isAdmin) {
		Coupon.findOneAndUpdate({couponCode: code},{couponDiscount: input.discount},{new:true})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
	else {
		return response.send("You are not an admin.");
		}
}

module.exports.archiveCoupon = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const input = request.body;

	if(userData.isAdmin) {
		Coupon.findOneAndUpdate({couponCode: input.code},{isActive: input.status},{new:true})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
	else {
		return response.send(false);
	}
}

module.exports.viewCoupon = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const input = request.body;


	if(userData!==null) {
		Coupon.findOne({couponCode: input.couponCode,isActive: true})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
	else {
		return response.send(false);
	}
}
