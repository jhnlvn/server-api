const mongoose = require("mongoose");
const Profile = require("../Models/profilesSchema.js")
const auth = require("../auth.js");

/*
	PROFILE AREA
*/

module.exports.viewProfile = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	let newDetails = request.body;
	let user = userData._id;

	Profile.findOne({userId: user})
	.then(result => {
		if(result !== null){
			return response.send(result)
		}else{
			let newProfile = new Profile({
				userId: user,
				firstName: newDetails.firstName,
				lastName: newDetails.lastName,
				address: newDetails.address
			})

			newProfile.save()
			.then(save => response.send(save))
			.catch(error => response.send(false))
		}
	})
	.catch(error => response.send(false))
}

module.exports.editProfile = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const newDetails = request.body;
	let user = userData._id


	Profile.findOneAndUpdate({userId: user},{
		firstName: newDetails.firstName,
		lastName: newDetails.lastName,
		address: newDetails.address
	},{new:true})
	.then(result => response.send(result))
	.catch(error => response.send(false))
}
