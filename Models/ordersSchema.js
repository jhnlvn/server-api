const mongoose = require("mongoose");

const ordersSchema = new mongoose.Schema({
	userId : {
		type : String,
		required : true
	},
	products : [
		{
			productId : {
				type : String,
				required : true
			},
			quantity : {
				type : Number,
				default : 1
			},
			subTotal : {
				type : Number,
				default : null
			}
		}
	],
	name : {
		type: String,
		default : null
	},
	contact : {
		type: Number,
		default : null
	},
	address : {
		type: String,
		default : null
	},
	couponCode : {
		type: String,
		default : null
	},
	paymentMethod : {
		type: String,
		default : null
	},
	totalAmount : {
		type: Number,
		default : null
	},
	status : {
		type: String,
		default : null
	},
	purchasedOn : {
		type : Date,
		default : null//new Date()
	}
})

module.exports = mongoose.model("Order", ordersSchema);
