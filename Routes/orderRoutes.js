const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const orderController = require("../Controllers/orderController");

router.put("/add/:id", orderController.addToCart);
router.get("/status/:status", orderController.orderStatus);
router.put("/change/:order", orderController.changeStatus);
router.put("/view", orderController.viewOrder);
router.get("/all", auth.verify, orderController.allOrder);

module.exports = router;