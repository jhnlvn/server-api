const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const productController = require("../Controllers/productController");

router.post("/add", auth.verify, productController.addProduct);
router.get("/showarchives", productController.viewArchives);
router.get("/viewall", productController.viewProducts);

router.get("/category/:category", productController.viewCategory);
router.get("/item/:id", productController.viewItem);
router.put("/update/:id", auth.verify, productController.updateProduct);
router.put("/archive/:id", auth.verify, productController.archiveProduct);
router.get("/search/:itemName", productController.searchProducts);

module.exports = router;